package net.ubtuni.youtubeuploader.configurations;

import lombok.Data;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.List;

@Configuration
@EnableConfigurationProperties
@PropertySource(value = "classpath:client_properties.json")
@Data
public class ClientProperties {

    private Web web;

    @Data
    private class Web{

        private String client_id;
        private String project_id;
        private String auth_uri;
        private String token_uri;
        private String auth_provider_x509_cert_url;
        private String client_secret;
        private List<String> redirect_uris;
        private List<String> javascript_origins;
    }
}