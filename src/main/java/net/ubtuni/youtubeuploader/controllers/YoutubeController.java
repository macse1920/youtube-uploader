package net.ubtuni.youtubeuploader.controllers;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.googleapis.media.MediaHttpUploader;
import com.google.api.client.http.InputStreamContent;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.Video;
import com.google.api.services.youtube.model.VideoSnippet;
import com.google.api.services.youtube.model.VideoStatus;

@RestController
@RequestMapping(value = "video")
public class YoutubeController
{

	private static final String CLIENT_SECRETS = "client_properties.json";
	private static final Collection<String> SCOPES =
			Arrays.asList("https://www.googleapis.com/auth/youtube.upload");

	private static final String APPLICATION_NAME = "API code samples";
	private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();


	@Autowired
	ResourceLoader resourceLoader;

	public Credential authorize(final NetHttpTransport httpTransport) throws IOException
	{
		InputStream in = resourceLoader.getResource("classpath:" + CLIENT_SECRETS).getInputStream();

		GoogleClientSecrets clientSecrets =
				GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

		final GoogleAuthorizationCodeFlow flow =
				new GoogleAuthorizationCodeFlow.Builder(httpTransport, JSON_FACTORY, clientSecrets, SCOPES)
						.setDataStoreFactory(
								new FileDataStoreFactory(new java.io.File("credentials/credentials")))
						.setAccessType("offline")
						.build();

		Credential credential =
				new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
		return credential;
	}

	public YouTube getService() throws GeneralSecurityException, IOException
	{
		final NetHttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
		Credential credential = authorize(httpTransport);
		return new YouTube.Builder(httpTransport, JSON_FACTORY, credential)
				.setApplicationName(APPLICATION_NAME)
				.build();
	}

	@RequestMapping(value = "upload", method = RequestMethod.POST)
	public ResponseEntity upload(@RequestParam("file") String videoPath, @RequestParam(value = "title") String title,
			@RequestBody String content)
			throws GeneralSecurityException, IOException
	{
		YouTube youtubeService = getService();

		File file = new File(videoPath);

		InputStreamContent mediaContent =
				new InputStreamContent("video/*",
						new BufferedInputStream(FileUtils.openInputStream(file)));

		Video videoObjectDefiningMetadata = new Video();

		VideoStatus status = new VideoStatus();
		status.setPrivacyStatus("public");
		videoObjectDefiningMetadata.setStatus(status);

		VideoSnippet snippet = new VideoSnippet();

		String videoTitle = String.format("Tech news - %s", title);

		if (videoTitle.length() > 60)
		{
			videoTitle = videoTitle.substring(0, 50).concat("...");
		}

		snippet.setTitle(videoTitle);

		String videoContent = getContent(content);

		if (videoContent.length() > 990)
		{
			videoContent = content.substring(0, 990).concat("...");
		}

		snippet.setDescription(videoContent);

		videoObjectDefiningMetadata.setSnippet(snippet);

		YouTube.Videos.Insert videoInsert = youtubeService.videos()
				.insert("snippet,statistics,status", videoObjectDefiningMetadata, mediaContent);

		MediaHttpUploader uploader = videoInsert.getMediaHttpUploader();

		uploader.setDirectUploadEnabled(false);

		return ResponseEntity.ok().body(videoInsert.execute().getId());

	}

	private String getContent(String content)
	{
		StringBuilder contentBuilder = new StringBuilder();

		contentBuilder.append("CONTENT")
				.append("\n")
				.append(content)
				.append("\n")
				.append("UPLOADED BY TECH NEWS CHANNEL - UBT ");

		return contentBuilder.toString();
	}
}